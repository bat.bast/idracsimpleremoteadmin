package org.boiteataquets.idrac.simpleremoteadmin.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Data
@Slf4j
public class ServerResponse {
    private String message;
    private int code;

    public Optional<String> toJson() {
        String res = null;
        try {
            return Optional.of(new ObjectMapper().writeValueAsString(this));
        } catch (Exception ex) {
            log.error("Cannot transform {}/{} to JSON", getMessage(), getCode());
        }
        return Optional.empty();
    }
}
