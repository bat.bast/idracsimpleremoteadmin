package org.boiteataquets.idrac.simpleremoteadmin.bean;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.boiteataquets.idrac.simpleremoteadmin.model.ServerResponse;
import org.boiteataquets.idrac.simpleremoteadmin.utils.NetworkCommands;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

@Component("applicationServerBean")
@Scope("singleton")
@Slf4j
public class ApplicationServerBean {
    private long lastExecutionCommandTime = 0;
    private String lastExecutionCommandName;

    @Setter
    private String applicationServer;

    @Setter
    private int applicationPort;

    public Optional<String> checkTCPPort() {
        return execute("checkTCPPort", 1);
    }

    private Optional<String> execute(String command, int delay) {
        ServerResponse sr = new ServerResponse();
        try {
            long currentTime = System.currentTimeMillis();
            if (lastExecutionCommandTime == 0 || currentTime - lastExecutionCommandTime >= delay * 1000) {
                lastExecutionCommandTime = currentTime;
                lastExecutionCommandName = command;
            } else {
                long waitingDelay = (lastExecutionCommandTime + delay * 1000 - currentTime) / 1000;
                String exceptionMessage = String.format(
                        "Too short delay between 2 commands: wait for %s'' ; previous = %s at %s, current = %s at %s",
                        waitingDelay,
                        lastExecutionCommandName, new Date(lastExecutionCommandTime),
                        command, new Date(currentTime));
                throw new Exception(exceptionMessage);
            }
            switch (command) {
                default:
                    sr.setMessage(NetworkCommands.checkTCPPort(applicationServer, applicationPort));
                    break;
            }
            sr.setCode(0);
        } catch (Exception ex) {
            String message = "Cannot " + command + " server: " + ex.getMessage();
            log.error(message);
            sr.setMessage(message);
            sr.setCode(1);
        }
        return sr.toJson();
    }
}
