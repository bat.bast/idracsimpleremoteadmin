package org.boiteataquets.idrac.simpleremoteadmin.bean;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.boiteataquets.idrac.simpleremoteadmin.model.ServerResponse;
import org.boiteataquets.idrac.simpleremoteadmin.utils.SMCLPCommands;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;

@Component("iDracServerBean")
@Scope("singleton")
@Slf4j
public class IDracServerBean {
    private long lastExecutionCommandTime = 0;
    private String lastExecutionCommandName;

    @Setter
    private String iDracServer;

    @Setter
    private int iDracSSHPort;

    @Setter
    private String iDracUser;

    @Setter
    private String iDracPassword;

    public Optional<String> status() {
        return execute("status", 30);
    }

    public Optional<String> ping() {
        return execute("ping", 10);
    }

    public Optional<String> start() {
        return execute("start", 60);
    }

    public Optional<String> stop() {
        return execute("stop", 60);
    }

    public Optional<String> reset() {
        return execute("reset", 60);
    }

    private Optional<String> execute(String command, int delay) {
        ServerResponse sr = new ServerResponse();
        try {
            long currentTime = System.currentTimeMillis();
            if (lastExecutionCommandTime == 0 || currentTime - lastExecutionCommandTime >= delay * 1000) {
                lastExecutionCommandTime = currentTime;
                lastExecutionCommandName = command;
            } else {
                long waitingDelay = (lastExecutionCommandTime + delay * 1000 - currentTime) / 1000;
                String exceptionMessage = String.format(
                        "Too short delay between 2 commands: wait for %s'' ; previous = %s at %s, current = %s at %s",
                        waitingDelay,
                        lastExecutionCommandName, new Date(lastExecutionCommandTime),
                        command, new Date(currentTime));
                throw new Exception(exceptionMessage);
            }
            switch (command) {
                case "start":
                    sr.setMessage(SMCLPCommands.start(iDracUser, iDracPassword, iDracServer, iDracSSHPort));
                    break;
                case "stop":
                    sr.setMessage(SMCLPCommands.stop(iDracUser, iDracPassword, iDracServer, iDracSSHPort));
                    break;
                case "reset":
                    sr.setMessage(SMCLPCommands.reset(iDracUser, iDracPassword, iDracServer, iDracSSHPort));
                    break;
                case "status":
                    sr.setMessage(SMCLPCommands.powerStatus(iDracUser, iDracPassword, iDracServer, iDracSSHPort));
                    break;
                default:
                    sr.setMessage(SMCLPCommands.ping(iDracUser, iDracPassword, iDracServer, iDracSSHPort));
                    break;
            }
            sr.setCode(0);
        } catch (Exception ex) {
            String message = "Cannot " + command + " server: " + ex.getMessage();
            log.error(message);
            sr.setMessage(message);
            sr.setCode(1);
        }
        return sr.toJson();
    }
}
