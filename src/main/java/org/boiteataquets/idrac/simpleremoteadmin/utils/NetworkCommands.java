package org.boiteataquets.idrac.simpleremoteadmin.utils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;

public abstract class NetworkCommands {
    private static int timeout = 2000;

    public static String checkTCPPort(String host, int port) throws IOException {
        SocketAddress socketAddress = new InetSocketAddress(host, port);
        Socket socket = new Socket();
        try {
            socket.connect(socketAddress, timeout);
            return "Started";
        } catch (SocketTimeoutException exception) {
            return "Stopped";
        } catch (IOException exception) {
            return "Stopped";
        } finally {
            socket.close();
        }
    }
}

