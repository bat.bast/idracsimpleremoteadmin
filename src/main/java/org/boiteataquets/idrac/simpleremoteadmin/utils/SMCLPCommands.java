package org.boiteataquets.idrac.simpleremoteadmin.utils;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;

public abstract class SMCLPCommands {

    public static String start(String username, String password,
                               String host, int port) throws Exception, AssertionError {
        String res = executeCommandBySSH(username, password, host, port, "start /system1");
        return res.split("\n", 0)[0].trim();
    }

    public static String stop(String username, String password,
                              String host, int port) throws Exception, AssertionError {
        String res = executeCommandBySSH(username, password, host, port, "stop /system1");
        return res.split("\n", 0)[0].trim();
    }

    public static String reset(String username, String password,
                               String host, int port) throws Exception {
        String res = executeCommandBySSH(username, password, host, port, "reset /system1");
        return res.split("\n", 0)[0].trim();
    }

    public static String powerStatus(String username, String password,
                                             String host, int port) throws Exception, AssertionError {
        String status = null;
        String res = executeCommandBySSH(username, password, host, port, "show -d properties=(EnabledState,powerstate) /system1");
        String[] lines = res.split("\n", 4);
        Hashtable<String, String> properties = new Hashtable<>(2);
        for (int i = 2; i <= 3; i++) {
            String line = lines[i].trim();
            String[] keyValue = line.split("=");
            String key = keyValue[0].trim();
            String value = keyValue[1].trim();
            properties.put(key, value);
        }
        if (properties.get("EnabledState").equals("2 (Enabled)") && properties.get("powerstate").equals("2 (On)")) {
            status = "Started";
        } else if (properties.get("EnabledState").equals("3 (Disabled)") && properties.get("powerstate").equals("13 (Off - Hard Graceful)")) {
            status = "Stopped";
        } else {
            status = "Unknown";
        }
        return status;
    }

    public static String ping(String username, String password,
                              String host, int port) throws Exception, AssertionError {
        String res = executeCommandBySSH(username, password, host, port, "show -d properties=(does_not_exist) /system1");
        return res.split("\n", 0)[0].trim();
    }

    private static String executeCommandBySSH(String username, String password,
                                              String host, int port, String command) throws Exception, AssertionError {

        Session session = null;
        ChannelExec channel = null;

        try {
            session = new JSch().getSession(username, host, port);
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();

            channel = (ChannelExec) session.openChannel("exec");
            channel.setCommand(command);
            ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
            channel.setOutputStream(responseStream);
            channel.connect();

            while (channel.isConnected()) {
                Thread.sleep(100);
            }

            String result = responseStream.toString();
            assert result.contains("/system1");
            assert !result.contains("COMMAND EXECUTION FAILED") : result;
            return result;
        } finally {
            if (session != null) {
                session.disconnect();
            }
            if (channel != null) {
                channel.disconnect();
            }
        }
    }
}
