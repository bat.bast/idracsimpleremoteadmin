package org.boiteataquets.idrac.simpleremoteadmin.controller;

import lombok.Getter;
import org.boiteataquets.idrac.simpleremoteadmin.bean.ApplicationServerBean;
import org.boiteataquets.idrac.simpleremoteadmin.bean.IDracServerBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
public class CommandController {
    @Autowired
    @Getter
    private IDracServerBean iDracServerBean;

    @Autowired
    @Getter
    private ApplicationServerBean applicationServerBean;

    @GetMapping("/")
    public String index() {
        return status();
    }

    @GetMapping("/applicationStatus")
    public String applicationStatus() {
        return getApplicationServerBean().checkTCPPort().get();
    }

    @GetMapping("/ping")
    public String ping() {
        return getIDracServerBean().ping().get();
    }

    @GetMapping("/status")
    public String status() {
        return getIDracServerBean().status().get();
    }

    @GetMapping("/start")
    public String start() {
        return getIDracServerBean().start().get();
    }

    @GetMapping("/stop")
    public String stop() {
        return getIDracServerBean().stop().get();
    }

    @GetMapping("/reset")
    public String reset() {
        return getIDracServerBean().reset().get();
    }
}
