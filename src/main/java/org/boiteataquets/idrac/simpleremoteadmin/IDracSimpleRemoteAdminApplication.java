package org.boiteataquets.idrac.simpleremoteadmin;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.boiteataquets.idrac.simpleremoteadmin.bean.ApplicationServerBean;
import org.boiteataquets.idrac.simpleremoteadmin.bean.IDracServerBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
@Slf4j
@Getter
@Setter
@EnableEncryptableProperties
public class IDracSimpleRemoteAdminApplication implements CommandLineRunner {
    // Properties from application.properties
    @Value("${application.server}")
    private String applicationServer;

    @Value("${application.port}")
    private int applicationPort;

    @Value("${idrac.server}")
    private String iDracServer;

    @Value("${idrac.ssh.port}")
    private int iDracSSHPort;

    @Value("${idrac.user}")
    private String iDracUser;

    @Value("${idrac.password}")
    private String iDracPassword;

    @Autowired
    @Getter
    private IDracServerBean iDracServerBean;

    @Autowired
    @Getter
    private ApplicationServerBean applicationServerBean;


    public static void main(String[] args) {
        SpringApplication.run(IDracSimpleRemoteAdminApplication.class, args);
    }

    @Override
    public void run(String[] args) throws IOException {
        log.info("Starting application");
        buildIDracServer();
        buildApplicationServer();
    }

    private void buildIDracServer() {
        getIDracServerBean().setIDracServer(getIDracServer());
        getIDracServerBean().setIDracSSHPort(getIDracSSHPort());
        getIDracServerBean().setIDracUser(getIDracUser());
        getIDracServerBean().setIDracPassword(getIDracPassword());
    }
    private void buildApplicationServer() {
        getApplicationServerBean().setApplicationServer(getApplicationServer());
        getApplicationServerBean().setApplicationPort(getApplicationPort());
    }
}
