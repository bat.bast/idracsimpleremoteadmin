import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class PasswordEncryption {
    @Test
    public void generateEncryptedValueWithJasypt() {
        PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword("MY_JASYP_PASSWORD"); // the master password, used to boot the application
        config.setAlgorithm("PBEWITHHMACSHA512ANDAES_256"); //The algorithm you want to use. This is the default one
        config.setKeyObtentionIterations("1000");
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setIvGeneratorClassName("org.jasypt.iv.RandomIvGenerator");
        config.setStringOutputType("base64");
        encryptor.setConfig(config);

        final String valueToBeEncrypted = "MY_PASSWORD";
        final String encryptedValue = encryptor.encrypt(valueToBeEncrypted);

        Assert.isTrue(valueToBeEncrypted.equals(encryptor.decrypt(encryptedValue)), "Password not match");

        System.out.println(encryptedValue); //The value to be set in ENC() into application.properties
    }
}
