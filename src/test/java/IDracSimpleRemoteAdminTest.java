import org.boiteataquets.idrac.simpleremoteadmin.IDracSimpleRemoteAdminApplication;
import org.boiteataquets.idrac.simpleremoteadmin.bean.ApplicationServerBean;
import org.boiteataquets.idrac.simpleremoteadmin.bean.IDracServerBean;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.util.Assert;

import java.util.Optional;

@SpringBootTest(classes = {IDracSimpleRemoteAdminApplication.class})
public class IDracSimpleRemoteAdminTest {

    @Autowired
    ApplicationContext appCtx;

    @Autowired
    private IDracServerBean iDracServerBean;

    @Autowired
    private ApplicationServerBean applicationServerBean;

    @Test
    public void pingServer() {
        Optional<String> res = iDracServerBean.ping();
        Assert.isTrue(res.get().contains("\"code\":0"), "Cannot ping server");
    }

    @Test
    public void statusServer() {
        Optional<String>  res = iDracServerBean.status();
        Assert.isTrue(res.get().contains("\"code\":0"), "Cannot get status server");
    }

    @Test
    public void startServer() {
        Optional<String>  res = iDracServerBean.start();
        Assert.isTrue(res.get().contains("\"code\": 0"), "Cannot start server");
    }

    @Test
    public void stopServer() {
        Optional<String>  res = iDracServerBean.stop();
        Assert.isTrue(res.get().contains("\"code\":0"), "Cannot stop server");
    }

    @Test
    public void pingServer2TimesWithTooShortDelay() throws Exception {
        Optional<String> res = iDracServerBean.ping();
        Assert.isTrue(res.get().contains("\"code\":0"), "Cannot ping server");
        Thread.sleep(10001);
        res = iDracServerBean.ping();
        Assert.isTrue(res.get().contains("\"code\":1"), "Cannot ping server");
    }

    @Test
    public void checkApplicationServer() {
        Optional<String> res = applicationServerBean.checkTCPPort();
        Assert.isTrue(res.get().contains("Started"), "Cannot ping server");
    }
}
